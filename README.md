# Hazelcast

## 项目背景

[**Hazelcast**](https://github.com/hazelcast/hazelcast)是一个分布式计算和存储平台，针对事件流的低延迟查询、聚合和有状态计算，和传统的数据源。它能让你快速建立高效的资源实时应用程序。您可以从小型边缘设备以任何规模部署它，或者部署到大型云实例集群。Hazelcast节点集群共享数据存储和计算负载。它可以动态地放大和缩小。向集群中添加新节点时，在集群中自动重新平衡当前运行的数据计算任务(称为作业)对其状态进行快照并进行伸缩处理担保。

## 扩展功能说明

### OBS、ECS

| 扩展对象                                                | 部署与使用 | 开发经历 |
|-----------------------------------------------------|-------|-------|
| [OBS](https://support.huaweicloud.com/obs/index.html) |   [OBS部署与使用](hazelcast-obs/README.md)   |   [OBS开发过程](hazelcast-obs/doc/hazelcast-obs.docx)   |
| [ECS](https://support.huaweicloud.com/ecs/index.html) |   [ECS部署与使用](hazelcast-ecs/README.md)   |   [ECS开发思路](https://gitee.com/HuaweiCloudDeveloper/Huaweicloud-Hazelcast-IMDG-plugin/blob/master-dev/docs/learning_report.docx)   |

## 云商店部署  

 [云商店部署链接](https://marketplace.huaweicloud.com/contents/d1fe7103-e3e7-4fb9-8ed7-38874c6d3e6a#productid=OFFI895249621104836608)