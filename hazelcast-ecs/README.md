# Hazelcast与华为云ECS的扩展

# 项目介绍

## 什么是Hazelcast

Hazelcast是一个分布式计算和存储平台，针对事件流的低延迟查询、聚合和有状态计算，和传统的数据源。它能让你快速建立高效的资源实时应用程序。您可以从小型边缘设备以任何规模部署它，或者部署到大型云实例集群。

Hazelcast节点集群共享数据存储和计算负载。它可以动态地放大和缩小。向集群中添加新节点时，在集群中自动重新平衡当前运行的数据计算任务(
称为作业)，对其状态进行快照并进行伸缩处理担保。

## 扩展功能

Hazelcast提供了三种方式组建集群

+ TCP/IP
+ 多播
+ 其他云服务发现方式

我们就是使用华为云ECS的云服务发现方式，使Hazelcast在ECS环境下组成集群

## 扩展经历

#### 初步构建开发思路，明确最终目标

经过初步分析友商代码，调用云服务API最终目的在于获取到某个服务器的IP地址

#### 分析代码共同部分，抽取重点类及方法

重点在于如何调用相关API获取IP信息以及开发DiscoveryStrategy及discoveryStrategyFactory的子类和实现类

#### 形成完整开发思路，依次完善代码任务

详细开发思路请点击 [doc文档](https://gitee.com/HuaweiCloudDeveloper/Huaweicloud-Hazelcast-IMDG-plugin/blob/master-dev/docs/learning_report.docx)。

# 源码编译部署

## 源码下载

[Hazelcast源码下载（github）](https://github.com/hazelcast/hazelcast)

## 插件集成

1. 将本仓库hazelcast-ecs下的 _huaweicloud文件夹_ 整个加入到源码/hazelcast/src/main/java/com/hazelcast/下，如下图所示：

![图片](/hazelcast-ecs/images/plugins.png)

2. 在源码/hazelcast/src/main/resources/META-INF/services/com.hazelcast.spi.discovery.DiscoveryStrategyFactory文件中加入 _com.hazelcast.huaweicloud.HwDiscoveryStrategyFactory_

![图片](/hazelcast-ecs/images/add.png)

3. 在根目录的pom文件中需要新增Lisense：Bouncy Castle Licence和GPLv2 with Classpath Exception

```xml
<includedLicenses>
    <includedLicense>CC0</includedLicense>
    <includedLicense>Apache License, Version 2.0</includedLicense>
    <includedLicense>MIT License</includedLicense>
    <includedLicense>Public Domain</includedLicense>
    <includedLicense>EPL 2.0</includedLicense>
    <includedLicense>Eclipse Distribution License - v 1.0</includedLicense>
    <includedLicense>BSD 3-Clause License</includedLicense>
    <includedLicense>BSD 2-Clause License</includedLicense>
    <includedLicense>The JSON License</includedLicense>
    <includedLicense>Bouncy Castle Licence</includedLicense>
    <includedLicense>GPLv2 with Classpath Exception</includedLicense>
</includedLicenses>
```   

4. 编译过程中可能会遇到 _manifest错误_，如下图所示：

![错误](/hazelcast-ecs/images/manifest-error.png)

解决方法： 源码中maven-bundle-plugin版本是2.4.0，将该插件版本升级为3.3.0，完美解决。

/hazelcast/pom.xml：

![解决](/hazelcast-ecs/images/bundle-version.png)

/pom.xml：

![解决](/hazelcast-ecs/images/bundle-version1.png)


## 使用Maven打包

使用mvn clean package 命令进行打包  
如果发现测试类异常使用mvn clean package -Dquick只打包不测试

# 相关配置介绍与启动

## 配置文件介绍

路径：resources/hazelcast-default.xml  
这是修改过的默认配置文件，打包后会形成hazelcast.xml,我们使用discovery-strategies的方式设置参数  
**注意：在38行设置集群名称**

## 与ECS交互的三种方式

+ 使用华为云提供的AK和SK，并填写项目id，服务器id和地区id
+ 使用华为云的IAM认证服务，填写iamDomain，iamUser，iamPassword以及项目id，服务器id和地区id
+ 使用服务器的元数据接口

#### 配置参数详情　

```xml
<properties>
    <!--    服务器id-->
    <property name="instance-id"></property>
    <!--    项目id-->
    <property name="project-id"></property>
    <!--    区域id-->
    <property name="region"></property>
    <!--    端口号-->
    <property name="port"></property>
    <!--    ak-->
    <property name="access-key"></property>
    <!--    sk-->
    <property name="secret-key"></property>
    <!--    iam账号-->
    <property name="iam-domain"></property>
    <!--    iam用户名-->
    <property name="iam-user"></property>
    <!--    iam密码-->
    <property name="iam-password"></property>
    <!--    tag-key-->
    <property name="tag-key"></property>
    <!--    tag-value-->
    <property name="tag-value"></property>
    <!--    服务器是否已启用-->
    <property name="instance-metadata-available">false</property>
</properties>
```

选择方式一或方式二，均需要填写instance-id，project-id和region
而后根据方式的不同，选择填写access-key和secret-key或iam-domain，iam-user和iam-password。  
如果选择方式三，则只需要将instance-metadata-available设置为true
**在所有的方式中，port字段为必填**  
方式一示例：
```xml
<properties>
    <!--    服务器id-->
    <property name="instance-id">YOUR-INSTANCE-ID</property>
    <!--    项目id-->
    <property name="project-id">YOUR-PROJECT-ID</property>
    <!--    区域id-->
    <property name="region">YOUR-REGION</property>
    <!--    端口号-->
    <property name="port">5701-5703</property>
    <!--    ak-->
    <property name="access-key">YOUR-ACCESS-KEY</property>
    <!--    sk-->
    <property name="secret-key">YOUR-SECRET-KEY</property>
    <!--    tag-key-->
    <property name="tag-key"></property>
    <!--    tag-value-->
    <property name="tag-value"></property>
    <!--    服务器是否已启用-->
    <property name="instance-metadata-available">false</property>
</properties>
```
方式二示例：
```xml
<properties>
    <!--    服务器id-->
    <property name="instance-id">YOUR-INSTANCE-ID</property>
    <!--    项目id-->
    <property name="project-id">YOUR-PROJECT-ID</property>
    <!--    区域id-->
    <property name="region">YOUR-REGION</property>
    <!--    端口号-->
    <property name="port">5701-5703</property>
    <!--    iam账号-->
    <property name="iam-domain">IAM-DOMAIN</property>
    <!--    iam用户名-->
    <property name="iam-user">IAM-USER</property>
    <!--    iam密码-->
    <property name="iam-password">IAM-PASSWORD</property>
    <!--    tag-key-->
    <property name="tag-key"></property>
    <!--    tag-value-->
    <property name="tag-value"></property>
    <!--    服务器是否已启用-->
    <property name="instance-metadata-available">false</property>
</properties>
```
方式三示例：
```xml
<properties>
    <!--    端口号-->
    <property name="port">5701-5703</property>
    <!--    服务器是否已启用-->
    <property name="instance-metadata-available">true</property>
</properties>
```
#### TAG字段的填写(可以不填)

tag-key和tag-value是用以查找相关服务器，如果您的服务器设置了tag值，并且在组成集群时想要将设了相同TAG的服务器组成一起，则可以填写tag-key，一般不需要填写tag-value，仅在做第二层匹配时需要填写

#### port字段的填写(必填)

port是指Hazelcast启动的端口号，默认为5701-5703，可以仅填写数字为5701或者使用‘-’连接范围，例如‘5701’或‘5701-5708’

## 不同使用场景下的配置方式

### 使用单独一台服务器，以不同端口号组建集群

推荐使用方式三，仅需将instance-metadata-available设置为true，并填写端口号，例如需要3个端口，就设置为‘5701-5703’

### 使用多台服务器，每台仅创建一个Hazelcast实例

推荐使用方式一，所有服务器的配置均保持相同，为其中某一台服务器的实例id，项目id及地域，且端口号设置成5701

### 使用多台服务器，每台创建多个Hazelcast实例

推荐使用方式一，所有服务器的配置均保持相同，为其中某一台服务器的实例id，项目id及地域，且端口号设置成最大的端口号

## 不同云服务之间的配置方式

### 同区域同VPC组成集群

参考上述各个场景下的配置方式

### 同区域不同VPC组成集群

参考情况1配置，需要在不同VPC之间建立对等连接  
点击查看如何[实现跨VPC的网络互通](https://support.huaweicloud.com/ecs_faq/ecs_faq_1339.html#section1)

### 不同区域组成集群

参考情况1配置，需要在不同区域之间建立云连接  
点击查看如何[实现跨区域的网络互通](https://support.huaweicloud.com/qs-cc/cc_02_0201.html)

**所有的集群连接均是建立在服务器之间正常通信的前提，目前仅可使用私网IP进行集群连接**