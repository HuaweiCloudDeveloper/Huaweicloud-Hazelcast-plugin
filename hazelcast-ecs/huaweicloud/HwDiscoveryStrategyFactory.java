/*
 * Copyright (c) 2008-2023, Hazelcast, Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hazelcast.huaweicloud;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.config.properties.PropertyDefinition;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.logging.ILogger;
import com.hazelcast.spi.discovery.DiscoveryNode;
import com.hazelcast.spi.discovery.DiscoveryStrategy;
import com.hazelcast.spi.discovery.DiscoveryStrategyFactory;
import com.hazelcast.spi.utils.RestClient;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Factory class which returns {@link HwDiscoveryStrategy} to Discovery SPI
 */
public class HwDiscoveryStrategyFactory implements DiscoveryStrategyFactory {

    @Override
    public Class<? extends DiscoveryStrategy> getDiscoveryStrategyType() {
        return HwDiscoveryStrategy.class;
    }

    @Override
    public DiscoveryStrategy newDiscoveryStrategy(DiscoveryNode node, ILogger logger,
            Map<String, Comparable> properties) {
        return new HwDiscoveryStrategy(properties);
    }

    @Override
    public Collection<PropertyDefinition> getConfigurationProperties() {
        List<PropertyDefinition> result = new ArrayList<>();
        for (HwProperties property : HwProperties.values()) {
            result.add(property.getDefinition());
        }
        return result;
    }

    /**
     * Checks if Hazelcast is running on Ecs
     * Client uses a regular heartbeat to detect if the cluster is still running.
     * Besides,we still need to make an API call to a local, non-routable address
     * http://169.254.169.254/openstack/latest/meta_data.json.
     */
    @Override
    public boolean isAutoDetectionApplicable() {
        return ecsInstanceMetadataAvailable() && hzInstanceIsRunning();
    }

    private boolean hzInstanceIsRunning() {
        HazelcastInstance hzInstance = HazelcastClient.newHazelcastClient();
        return hzInstance.getLifecycleService().isRunning();
    }

    // Check whether http://169.254.169.254/openstack/latest/meta_data.json is
    // available
    private boolean ecsInstanceMetadataAvailable() {
        return isEndpointAvailable("http://169.254.169.254/openstack/latest/meta_data.json");
    }

    static boolean isEndpointAvailable(String url) {
        return !RestClient.create(url).withConnectTimeoutSeconds(1).withReadTimeoutSeconds(1).withRetries(1)
                .withHeader("Metadata", "True").get().getBody().isEmpty();
    }

    @Override
    public DiscoveryStrategyLevel discoveryStrategyLevel() {
        return DiscoveryStrategyLevel.CLOUD_VM;
    }

}
