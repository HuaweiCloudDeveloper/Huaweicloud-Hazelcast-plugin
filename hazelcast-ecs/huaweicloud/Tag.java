/*
 * Copyright (c) 2008-2023, Hazelcast, Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hazelcast.huaweicloud;

import com.hazelcast.config.InvalidConfigurationException;

/**
 * when EcsApi with Tags, you will find Server for this tag
 */
class Tag {
    private final String key;
    private final String value;

    Tag(String key, String value) {
        if (key == null) {
            throw new InvalidConfigurationException("Tag requires at least key");
        }
        this.key = key;
        if (value == null) {
            this.value = "";
        } else {
            this.value = value;
        }

    }

    String getKey() {
        return key;
    }

    String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Tag tag = (Tag) o;

        if (!key.equals(tag.key)) {
            return false;
        }
        return value.equals(tag.value);
    }

    @Override
    public int hashCode() {
        int result = key.hashCode();
        result = 31 * result + value.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return String.format("(key=%s, value=%s)", key, value);
    }
}
