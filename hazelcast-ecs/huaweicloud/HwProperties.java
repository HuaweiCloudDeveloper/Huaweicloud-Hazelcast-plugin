/*
 * Copyright (c) 2008-2023, Hazelcast, Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hazelcast.huaweicloud;

import com.hazelcast.config.properties.PropertyDefinition;
import com.hazelcast.config.properties.PropertyTypeConverter;
import com.hazelcast.config.properties.SimplePropertyDefinition;

import static com.hazelcast.config.properties.PropertyTypeConverter.BOOLEAN;
import static com.hazelcast.config.properties.PropertyTypeConverter.STRING;

/**
 * Configuration properties for the Hazelcast Discovery Plugin for Ecs
 * We prioritize using ak and sk for identity authentication.
 * If users do not fill in ak and sk, they can obtain tokens based on iam
 * information.
 * If none are filled in, a temporary ak and sk will be obtained through server
 * metadata,
 * and true must be selected in the instanceMetadataAvailable field
 * Port has default value 5701-5703
 * You can find the corresponding server by setting tag key and value
 * All other parameters are mandatory(instanceId,projectId,region)
 */
public enum HwProperties {

    /**
     * Access key of your account on ECS
     */
    ACCESS_KEY("access-key", STRING, true),

    /**
     * Secret key of your account on ECS
     */
    SECRET_KEY("secret-key", STRING, true),
    /**
     * IAMDomain from your IAM
     */
    IAM_DOMAIN("iam-domain", STRING, true),
    /**
     * IAMUser from your IAM
     */
    IAM_USER("iam-user", STRING, true),
    /**
     * IAMPassword from your IAM
     */
    IAM_PASSWORD("iam-password", STRING, true),
    /**
     * The id of your ECS Server
     */
    INSTANCE_ID("instance-id", STRING, true),
    /**
     * The region of your ECS Server
     */
    REGION("region", STRING, true),
    /**
     * use ',' split like "a,b,c"
     */
    TAG_KEY("tag-key", STRING, true),
    /**
     * use ',' split like "a,b,c";key and value must one by one
     */
    TAG_VALUE("tag-value", STRING, true),
    /**
     * default port is 5701-5703,you can use '-' to fill some port
     */
    PORT("port", STRING, true),
    /**
     * project id of your service
     */
    PROJECT_ID("project-id", STRING, true),
    /**
     * if your metadata is available
     */
    INSTANCE_METADATA_AVAILABLE("instance-metadata-available", BOOLEAN, true);

    private final PropertyDefinition propertyDefinition;

    HwProperties(String key, PropertyTypeConverter typeConverter, boolean optional) {
        this.propertyDefinition = new SimplePropertyDefinition(key, optional, typeConverter);
    }

    PropertyDefinition getDefinition() {
        return propertyDefinition;
    }
}
