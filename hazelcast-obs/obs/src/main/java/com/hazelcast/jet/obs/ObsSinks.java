/*
 * Copyright 2023 Hazelcast Inc.
 *
 * Licensed under the Hazelcast Community License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://hazelcast.com/hazelcast-community-license
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hazelcast.jet.obs;

import com.hazelcast.function.FunctionEx;
import com.hazelcast.function.SupplierEx;
import com.hazelcast.internal.util.ExceptionUtil;
import com.hazelcast.internal.util.StringUtil;
import com.hazelcast.jet.pipeline.Sink;
import com.hazelcast.jet.pipeline.SinkBuilder;
import com.hazelcast.memory.MemoryUnit;
import com.obs.services.IObsClient;

import com.obs.services.model.InitiateMultipartUploadRequest;
import com.obs.services.model.PartEtag;
import com.obs.services.model.UploadPartRequest;
import com.obs.services.model.CompleteMultipartUploadRequest;
import com.obs.services.model.AbortMultipartUploadRequest;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static com.hazelcast.internal.util.JVMUtil.upcast;

/**
 * Contains factory methods for creating HuaweiCloud OBS sinks.
 */
public final class ObsSinks {

    private ObsSinks() {
    }

    /**
     * Convenience for {@link #obs(String, String, Charset, SupplierEx, FunctionEx)}
     * Uses {@link Object#toString()} to convert the items to lines.
     */
    @Nonnull
    public static <T> Sink<? super T> obs(
            @Nonnull String bucketName,
            @Nonnull SupplierEx<? extends IObsClient> clientSupplier
    ) {
        return obs(bucketName, null, StandardCharsets.UTF_8, clientSupplier, Object::toString);
    }

    /**
     * Creates an HuaweiCloud OBS {@link Sink} which writes items to files into the
     * given bucket. Sink converts each item to string using given {@code
     * toStringFn} and writes it as a line. The sink creates a file
     * in the bucket for each processor instance. Name of the file will include
     * an user provided prefix (if defined) and processor's global index,
     * for example the processor having the
     * index 2 with prefix {@code my-object-} will create the object
     * {@code my-object-2}.
     * <p>
     * No state is saved to snapshot for this sink. If the job is restarted
     * previously written files will be overwritten.
     * <p>
     * The default local parallelism for this sink is 1.
     * <p>
     * Here is an example which reads from a map and writes the entries
     * to given bucket using {@link Object#toString()} to convert the
     * values to a line.
     *
     * <pre>{@code
     * Pipeline p = Pipeline.create();
     * p.readFrom(Sources.map("map"))
     *  .writeTo(ObsSinks.obs("bucket", "my-map-", StandardCharsets.UTF_8,
     *      () -> IObsClient.create(),
     *      Object::toString
     * ));
     * }</pre>
     *
     * @param <T>            type of the items the sink accepts
     * @param bucketName     the name of the bucket
     * @param prefix         the prefix to be included in the file name
     * @param charset        the charset to be used when encoding the strings
     * @param clientSupplier OBS client supplier
     * @param toStringFn     the function which converts each item to its
     *                       string representation
     */
    @Nonnull
    public static <T> Sink<? super T> obs(
            @Nonnull String bucketName,
            @Nullable String prefix,
            @Nonnull Charset charset,
            @Nonnull SupplierEx<? extends IObsClient> clientSupplier,
            @Nonnull FunctionEx<? super T, String> toStringFn

    ) {
        String charsetName = charset.name();
        return SinkBuilder
                .sinkBuilder("obsSink", context ->
                        new OBSSinkContext<>(bucketName, prefix, charsetName, context.globalProcessorIndex(),
                                toStringFn, clientSupplier))
                .<T>receiveFn(OBSSinkContext::receive)
                .flushFn(OBSSinkContext::flush)
                .destroyFn(OBSSinkContext::close)
                .build();
    }

    static final class OBSSinkContext<T> {

        static final int DEFAULT_MAXIMUM_PART_NUMBER = 10000;
        static final int MINIMUM_PART_NUMBER = 1;
        // visible for testing
        static int maximumPartNumber = DEFAULT_MAXIMUM_PART_NUMBER;

        // the minimum size required for each part in HuaweiCloud multipart
        static final int DEFAULT_MINIMUM_UPLOAD_PART_SIZE = (int) MemoryUnit.MEGABYTES.toBytes(5);
        static final double BUFFER_SCALE = 1.2d;

        private final String bucketName;
        private final String prefix;
        private final int processorIndex;
        private final IObsClient obsClient;
        private final FunctionEx<? super T, String> toStringFn;
        private final Charset charset;
        private final byte[] lineSeparatorBytes;
        private final List<PartEtag> completedParts = new ArrayList<>();

        private ByteBuffer buffer;
        private int partNumber = MINIMUM_PART_NUMBER; // must be between 1 and maximumPartNumber
        private int fileNumber;
        private String uploadId;

        private OBSSinkContext(
                String bucketName,
                @Nullable String prefix,
                String charsetName, int processorIndex,
                FunctionEx<? super T, String> toStringFn,
                SupplierEx<? extends IObsClient> clientSupplier) {
            this.bucketName = bucketName;
            String trimmedPrefix = StringUtil.trim(prefix);
            this.prefix = StringUtil.isNullOrEmpty(trimmedPrefix) ? "" : trimmedPrefix;
            this.processorIndex = processorIndex;
            this.obsClient = clientSupplier.get();
            this.toStringFn = toStringFn;
            this.charset = Charset.forName(charsetName);
            this.lineSeparatorBytes = System.lineSeparator().getBytes(charset);
            checkIfBucketExists();
            resizeBuffer(DEFAULT_MINIMUM_UPLOAD_PART_SIZE);
        }

        private void initiateUpload() {
            InitiateMultipartUploadRequest req = new InitiateMultipartUploadRequest(bucketName, key());
            uploadId = obsClient.initiateMultipartUpload(req).getUploadId();
        }

        private void checkIfBucketExists() {
            obsClient.getBucketLocation(bucketName);
        }

        private void receive(T item) {
            byte[] bytes = toStringFn.apply(item).getBytes(charset);
            int length = bytes.length + lineSeparatorBytes.length;

            // not enough space in buffer to write
            if (buffer.remaining() < length) {
                // we try to flush the current buffer first
                flush();
                // this might not be enough - either item is bigger than current
                // buffer size or there was not enough data in the buffer to upload
                // in this case we have to resize the buffer to hold more data
                if (buffer.remaining() < length) {
                    resizeBuffer(length + buffer.position());
                }
            }

            buffer.put(bytes);
            buffer.put(lineSeparatorBytes);
        }

        private void resizeBuffer(int minimumLength) {
            assert buffer == null || buffer.position() < minimumLength;

            int newCapacity = (int) (minimumLength * BUFFER_SCALE);
            ByteBuffer newBuffer = ByteBuffer.allocateDirect(newCapacity);
            if (buffer != null) {
                upcast(buffer).flip();
                newBuffer.put(buffer);
            }
            buffer = newBuffer;
        }

        private void flush() {
            if (uploadId == null) {
                initiateUpload();
            }
            if (buffer.position() > DEFAULT_MINIMUM_UPLOAD_PART_SIZE) {
                boolean isLastPart = partNumber == maximumPartNumber;
                flushBuffer(isLastPart);
            }
        }

        private void close() throws IOException {
            try {
                flushBuffer(true);
            } finally {
                obsClient.close();
            }
        }

        private void flushBuffer(boolean isLastPart) {
            if (buffer.position() > 0) {
                // switch read mode
                upcast(buffer).flip();
                // Get unread data and convert it to InputStream
                byte[] byteArray = new byte[buffer.remaining()];
                buffer.get(byteArray);
                InputStream inputStream = new ByteArrayInputStream(byteArray);
                UploadPartRequest req = new  UploadPartRequest(bucketName, key());
                req.setUploadId(uploadId);
                req.setPartNumber(partNumber);
                req.setInput(inputStream);
                completedParts.add(new PartEtag(obsClient.uploadPart(req).getEtag(), partNumber));
                partNumber++;
                upcast(buffer).clear();
            }

            if (isLastPart) {
                completeUpload();
            }
        }

        private void completeUpload() {
            try {
                if (completedParts.isEmpty()) {
                    abortUpload();
                } else {
                    CompleteMultipartUploadRequest req = new CompleteMultipartUploadRequest(bucketName, key(), uploadId, completedParts);
                    obsClient.completeMultipartUpload(req);
                    completedParts.clear();
                    partNumber = MINIMUM_PART_NUMBER;
                    uploadId = null;
                    fileNumber++;
                }
            } catch (Exception e) {
                abortUpload();
                ExceptionUtil.rethrow(e);
            }
        }

        private void abortUpload() {
            AbortMultipartUploadRequest abortMultipartUploadRequest = new AbortMultipartUploadRequest(bucketName, key(), uploadId);
            obsClient.abortMultipartUpload(abortMultipartUploadRequest);
        }

        private String key() {
            return prefix + processorIndex + (fileNumber == 0 ? "" : "." + fileNumber);
        }
    }
}

