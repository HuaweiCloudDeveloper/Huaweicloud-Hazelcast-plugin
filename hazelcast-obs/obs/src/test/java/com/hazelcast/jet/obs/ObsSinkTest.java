/*
 * Copyright 2023 Hazelcast Inc.
 *
 * Licensed under the Hazelcast Community License (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://hazelcast.com/hazelcast-community-license
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hazelcast.jet.obs;

import com.hazelcast.function.SupplierEx;
import com.hazelcast.logging.ILogger;
import com.hazelcast.logging.Logger;
import com.hazelcast.test.HazelcastSerialClassRunner;
import com.hazelcast.test.annotation.NightlyTest;
import com.obs.services.ObsClient;
import com.obs.services.model.ListObjectsRequest;
import com.obs.services.model.ObjectListing;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;

@Category({NightlyTest.class})
@RunWith(HazelcastSerialClassRunner.class)
public class ObsSinkTest extends ObsTestBase {

    private static final int WAIT_AFTER_CLEANUP_IN_SECS = 5;
    private static final String BUCKET = "jet-obs-connector-test-bucket-sink";
    private static final String prefix = randomString() + "/";
    private static final ILogger logger = Logger.getLogger(ObsSinkTest.class);


    @Test
    public void test() {
        testSink(BUCKET, prefix + "test-", 1000);
    }

    @Test
    public void when_writesToExistingFile_then_overwritesFile() {
        testSink(BUCKET, prefix + "write-existing-file-", 100);
        testSink(BUCKET, prefix + "write-existing-file-", 200);
    }

    @Test
    public void when_writeToNotExistingBucket() {
        testSinkWithNotExistingBucket("jet-s3-connector-test-bucket-sink-THIS-BUCKET-DOES-NOT-EXIST");
    }

    @Test
    public void when_withSpaceInName() {
        testSink(BUCKET, prefix + "file with space-", 10);
    }

    @Test
    public void when_withNonAsciiSymbolInName() {
        testSink(BUCKET, prefix + "non-ascii-name-测试-", 10);
    }

    @Test
    public void when_withNonAsciiSymbolInFile() {
        testSink(BUCKET, prefix + "non-ascii-file-", 10, "测试");
    }

    SupplierEx<ObsClient> clientSupplier() {
        return ObsSinkTest::client;
    }

    private static ObsClient client() {
        return new ObsClient("");
    }

    private static ObjectListing listObjects(ObsClient client) {
        ListObjectsRequest listObjectsRequest = new ListObjectsRequest(BUCKET);
        listObjectsRequest.setPrefix(prefix);
        return client.listObjects(listObjectsRequest);
    }


}
