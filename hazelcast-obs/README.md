# Hazelcast与华为云OBS的扩展

# 项目介绍

## 什么是Hazelcast

Hazelcast是一个分布式计算和存储平台，针对事件流的低延迟查询、聚合和有状态计算，和传统的数据源。它能让你快速建立高效的资源实时应用程序。您可以从小型边缘设备以任何规模部署它，或者部署到大型云实例集群。

## 扩展介绍

### ObsSinks

OBSSinks用于执行写入OBS操作。  

它提供了两个重载的方法名为"obs"，用于创建OBS Sink。  
第一个"obs"方法接受存储桶名称和一个供应商函数来创建OBS客户端作为输入参数。这是一个方便的方法，它调用第二个"obs"方法，
并使用其他参数的默认值。它使用toString()方法将项目转换为行。

第二个"obs"方法是创建OBS Sink的主要方法。它接受存储桶名称、前缀、字符集、客户端供应商函数和toString函数作为输入参数。
它创建一个名为"obsSink"的SinkBuilder，并配置它的接收函数、刷新函数和销毁函数。接收函数负责接收项目并将其写入OBS Sink。
刷新函数在缓冲区已满或Sink关闭时调用，将数据刷新到OBS Sink。销毁函数在Sink销毁时调用，关闭OBS客户端。

代码还定义了一个名为"OBSSinkContext"的嵌套类，表示OBS Sink的上下文。它包含了初始化上传、检查存储桶是否存在、接收项目、
调整缓冲区大小、刷新缓冲区以及完成或中止上传的方法。

### ObsSources

ObsSources提供了创建BatchSource的方法，该方法从OBS存储桶中读取对象，将每个对象转换为所需的输出对象，并将其发送到下游。

该代码包括以下方法：

1. obs（List＜String＞bucketNames，String prefix，SupplierEx＜？extends IObsClient＞clientSupplier）：  
此方法创建一个obs BatchSource，它使用给定的前缀列出bucket列表中的所有对象。它在不进行任何转换的情况下向下游发送行，并使用UTF-8编码。  

2. obs（List＜String＞bucketNames，String prefix，Charset Charset，SupplierEx＜？extends IObsClient＞clientSupplier，BiFunctionEx＜String，String，？e  xtends T>mapFn）：  
该方法创建一个obs BatchSource，使用给定的前缀从bucket列表中读取对象，逐行读取，使用提供的映射函数将每一行转换为所需的输出对象，并将其排放到下游。  

3. obs（List＜String＞bucketNames，String prefix，SupplierEx＜？extends IObsClient＞clientSupplier，FunctionEx＜？super InputStream，？extends Stream＜I＞＞readFileFn，BiFunctionEx＜String，？super I，？extend T＞mapFn）：  
该方法创建一个obs BatchSource，该方法使用给定的前缀列出bucket列表中的所有对象，并使用提供的readFileFn函数读取它们，使用提供的映射函数将每个读取的项目转换为所需的输出对象，并将其发送到下游。  

4. obs（List＜String＞bucketNames，String prefix，SupplierEx＜？extends IObsClient＞clientSupplier，TriFunction＜？super InputStream，String，String，？extends Stream＜I＞＞readFileFn，BiFunctionEx＜String，？super I，？extend T>mapFn）：  
该方法创建一个obs BatchSource，该方法使用给定的前缀列出bucket列表中的所有对象，并使用提供的readFileFn函数读取它们，使用提供的映射函数将每个读取的项目转换为所需的输出对象，并将其发送到下游。  

该代码还包括一个名为“OBSSourceContext”的私有内部类，用于实现Processor接口。这个类负责从OBS bucket中读取对象，对它们进行处理，并向下游发送转换后的对象。它使用OBS客户端与OBS服务进行交互。

# 源码编译部署

## 源码下载

[Hazelcast源码下载（github）](https://github.com/hazelcast/hazelcast)

## 插件集成

1. 将本仓库hazelcast-obs下的 _obs文件夹_ 整个加入到源码/extensions/下，如下图所示：

![图片](images/img_1.png)

2. 在根目录的pom文件的<licenseMerges>中合并License：Apache 2.0 License

```xml
<licenseMerge>
        Apache License, Version 2.0|Apache License v2.0|
        Apache Software License 2.0|The Apache Software License, Version 2.0|
        Apache License, version 2.0|The Apache Software License, version 2.0|
        Apache 2|Apache 2.0|Apache-2.0|Apache License 2.0|The Apache License, Version 2.0|
        Apache Software License - Version 2.0|Apache License Version 2.0|Apache 2.0 License
</licenseMerge>
```   

## 使用Maven打包

使用mvn clean package 命令进行打包  
如果发现测试类异常使用mvn clean package -Dquick只打包不测试

# 相关配置与使用

这里提供一个使用demo，仅供简单参考：

```java
package com.hazelcast.jet.obs;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.jet.pipeline.Pipeline;
import com.hazelcast.jet.pipeline.Sinks;
import com.hazelcast.jet.pipeline.Sources;
import com.hazelcast.logging.ILogger;
import com.hazelcast.logging.Logger;
import com.hazelcast.map.IMap;
import com.obs.services.ObsClient;

import java.nio.charset.Charset;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * @author: wengruichen
 * @since: 2023/8/21:16:58
 * @description: test
 */
public class ObsTest {
    /**
     * 字符集
     */
    private static final ILogger LOGGER = Logger.getLogger(ObsTest.class);
    private static final Charset CHARSET = UTF_8;
    /**
     * OBS AK
     */
    private static final String ACCESS_KEY = "your access key";
    /**
     * OBS SK
     */
    private static final String SECURITY_KEY = "your security key";
    /**
     * OBS URL
     */
    private static final String ENDPOINT = "obs.{region}.myhuaweicloud.com";
    /**
     * OBS bucket name
     */
    private static final String BUCKET_NAME = "bucket name";
    /**
     * OBS prefix
     */
    private static final String PREFIX = "prefix";

    public static void main(String[] str) {
        HazelcastInstance hz = Hazelcast.bootstrappedInstance();
        putData2ObsByMap(hz);
        getData2MapFromObs(hz);
    }

    /**
     * get data from hazelcast map and put them into obs
     *
     * @param hz HazelcastInstance
     */
    public static void putData2ObsByMap(final HazelcastInstance hz) {
        IMap<String, String> hzMap = hz.getMap("map");
        hzMap.put("key1", "value1");
        hzMap.put("key2", "value2");
        hzMap.put("key3", "value3");

        Pipeline p = Pipeline.create();
        p.readFrom(Sources.map(hzMap))
                .writeTo(ObsSinks.obs(BUCKET_NAME, PREFIX, CHARSET, () ->
                        new ObsClient(ACCESS_KEY, SECURITY_KEY, ENDPOINT), Object::toString));

        hz.getJet().newJob(p).join();
    }

    /**
     * get data from obs and put them into hazelcast map
     *
     * @param hz HazelcastInstance
     */
    public static void getData2MapFromObs(final HazelcastInstance hz) {
        IMap<String, String> hzMap = hz.getMap("map");

        List<String> nameList = new ArrayList<>();
        nameList.add(BUCKET_NAME);

        Pipeline p1 = Pipeline.create();
        p1.readFrom(ObsSources.obs(nameList, PREFIX, () -> new ObsClient(ACCESS_KEY, SECURITY_KEY, ENDPOINT)))
                .map(mapString -> {
                    String[] split = mapString.split("=");
                    String key = split[0];
                    String value = split[1];
                    LOGGER.info("Key: " + key + ", Value: " + value);
                    return new AbstractMap.SimpleEntry<>(key, value);
                }).writeTo(Sinks.map(hzMap));

        hz.getJet().newJob(p1).join();
    }

}

```
>该demo中具有6个静态私有参数，分别是：
>+ CHARSET 字符集，一般为UTF_8
>+ ACCESS_KEY 连接obs的ak
>+ SECURITY_KEY 连接obs的sk
>+ ENDPOINT obs访问路径，只需要将自己的区域替换字符串中的region即可，对应的region可参考
>+ BUCKET_NAME obs桶名
>+ PREFIX obs前缀名

>主要方法为putData2ObsByMap(hz)和getData2MapFromObs(hz)，下面简单介绍：
>1. putData2ObsByMap()：从hazelcast的map中获取数据并上传到obs中储存
>2. getData2MapFromObs()：从obs中获取数据并储存到hazelcast的map中

通过调用main方法，并对以上参数设置正确的数值，即可完成map数据的上传和下载。

**Hazelcast中如何调用该demo？**

具体方式可参考：[hazelcast官网文档](https://docs.hazelcast.com/hazelcast/latest/pipelines/stream-processing-client)



